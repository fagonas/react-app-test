import React, { Component } from 'react';
import {
  TextInput,
  StyleSheet,
  Text,
  View,
  Button,
  AppRegistry
} from 'react-native';

export default class TestComponent extends Component {

    onChange1(value) {
        console.log('text is changeing = ', value);


    }

    render() {
        return (
            <View>
                <TextInput
                    placeholder='enter text here'
                    onChangeText={this.onChange1}
                />
                <Button
                    onPress={()=> undefined}
                    title="Learn More"
                    color="#841584"
                    accessibilityLabel="Learn more about this purple button"
                />
            </View>
        );
    }
}

AppRegistry.registerComponent('TestComponent', () => TestComponent);